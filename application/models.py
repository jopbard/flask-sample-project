# MODELS.PY 
# set up db inside the __init__.py under src folder 
from application import db, login_manager
from werkzeug.security import generate_password_hash,check_password_hash
from flask_login import UserMixin 
import datetime 

@login_manager.user_loader 
def load_user(user_id):
    return User.query.get(user_id)
    
class User(db.Model,UserMixin):

    __tablename__ = 'users'
    id = db.Column(db.Integer,primary_key=True)
    email = db.Column(db.String(64),unique=True,index=True)
    username = db.Column(db.String(64),unique=True,index=True)
    password_hash = db.Column(db.String(128))
    registered_on = db.Column(db.DateTime, nullable=False)
    admin = db.Column(db.Boolean, nullable=False, default=False)
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    confirmed_on = db.Column(db.DateTime)

    def __init__(self,email,username,password):
        self.email = email 
        self.username = username 
        self.password_hash = generate_password_hash(password)
        self.registered_on = self.now()
        
    def check_password(self,password):
        return check_password_hash(self.password_hash,password)

    def now(self):
        return datetime.datetime.now()

class Kitten(db.Model):
    
    __tablename__ = 'kittens'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    owner = db.relationship('Owner', backref='kitten', uselist=False)
    def __init__(self, name):
        self.name = name 

    def __repr__(self):
        if self.owner:
            return f"Kitten name is {self.name} and owner is {self.owner.name}"
        else:
            return f"Kitten name: {self.name} and no owner assigned yet!"

class Owner(db.Model):

    __tablename__ = 'owners'

    id = db.Column(db.Integer,primary_key=True)
    name = db.Column(db.Text)
    kitten_id = db.Column(db.Integer,db.ForeignKey('kittens.id'))

    def __init__(self, name, kitten_id):
        self.name = name
        self.kitten_id = kitten_id

    def __repr__(self):
        return f"Owner name: {self.name}"