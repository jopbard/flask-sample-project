# forms.py kittens 
from flask_wtf import FlaskForm
from wtforms import StringField,IntegerField,SubmitField

class AddForm(FlaskForm):
    
    name = StringField('Name of Kitten: ')
    submit = SubmitField('Add Kitten')

class DelForm(FlaskForm):

    id = IntegerField("Id number of Kitten to remove: ")
    submit = SubmitField("Remove Kitten")