# application/kittens/views.py 
from flask import Blueprint,render_template,redirect,url_for
from application import db 
from application.kittens.forms import AddForm,DelForm 
from application.models import Kitten 

kittens_blueprint = Blueprint('kittens',__name__,
                                template_folder='templates/kittens')

@kittens_blueprint.route('/add', methods=['GET','POST'])
def add_kit():

    form = AddForm()

    if form.validate_on_submit():

        name = form.name.data 

        new_kitten = Kitten(name)
        db.session.add(new_kitten)
        db.session.commit()

        return redirect(url_for('kittens.list'))
    return render_template('add.html', form=form)

@kittens_blueprint.route('/list')
def list():
    
    kittens = Kitten.query.all()

    return render_template('list.html', kittens=kittens)

@kittens_blueprint.route('/delete', methods=['GET','POST'])
def del_kit():

    form = DelForm()

    if form.validate_on_submit():
        id = form.id.data 
        kitten = Kitten.query.get(id)

        db.session.delete(kitten)
        db.session.commit()

        return redirect(url_for('kittens.list'))
    return render_template('delete.html',form=form)