# __init__.py 
from flask import Flask 
from flask_sqlalchemy import SQLAlchemy 
from flask_migrate import Migrate 
from flask_login import LoginManager
from flask_mail import Mail, Message

login_manager = LoginManager()

app = Flask(__name__)

app.config['SECRET_KEY'] = 'asdfadsf'
app.config['SECURITY_PASSWORD_SALT'] = 'h4y4h4y'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root@localhost:8889/flaskk'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False 

app.config.update(
	DEBUG=True,
	#EMAIL SETTINGS
	MAIL_SERVER='smtp.gmail.com',
	MAIL_PORT=465,
	MAIL_USE_SSL=True,
	MAIL_USERNAME = 'glinkscraper@gmail.com',
	MAIL_PASSWORD = 'wsrgnxqbayvpxkep'
	)
mail = Mail(app)

db = SQLAlchemy(app)
Migrate(app, db)
#db.create_all()

## Initialize login_manager
login_manager.init_app(app)
login_manager.login_view = 'login'

from application.kittens.views import kittens_blueprint
from application.owners.views import owners_blueprint 
from application.users.views import users_blueprint

app.register_blueprint(kittens_blueprint,url_prefix='/kittens')
app.register_blueprint(owners_blueprint,url_prefix='/owners')
app.register_blueprint(users_blueprint,url_prefix='/users')