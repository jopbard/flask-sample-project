# applications/users/views.py 
from flask import Blueprint,render_template,redirect,url_for,flash,abort,request
from application import app,db,mail 
from application.token import generate_confirmation_token,confirm_token
from flask_login import login_user,login_required,logout_user
from application.models import User 
from application.users.forms import LoginForm,RegistrationForm
from flask_mail import Message
import datetime 

users_blueprint = Blueprint('users',__name__,
                                template_folder='templates/users')

def send_email_confirmation(to, html):
    msg = Message("Confirm Your Email",sender="glinkscraper@gmail.com",
                        recipients=[to], html=html)
    mail.send(msg)
        
@users_blueprint.route('/list',methods=['GET','POST'])
@login_required
def list():
    pass 

@users_blueprint.route('/init')
def init():
    db.create_all()
    return 'Done'

@users_blueprint.route('/send_mail')
def send_mail():
    try:
        msg = Message("Send Mail Tutorial!",sender="glinkscraper@gmail.com",recipients=["kanor.mang22@gmail.com"])
        msg.body = "Yo!\nGAGO"
        #msg.html = render_template('send_mail.html')
        mail.send(msg)

        return 'Mail sent!'
    except Exception as e:
        
        return(str(e)) 

@users_blueprint.route('/logout')
@login_required
def logout():
    logout_user()
    flash('You logged out!')
    return redirect(url_for('kittens.list'))

@users_blueprint.route('/login',methods=['GET','POST'])
def login():

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()

        if user.check_password(form.password.data) and user is not None:

            login_user(user)
            flash('Logged in Successfully!')

            next = request.args.get('next')

            if next == None or not next[0] == '/':
                next = url_for('kittens.list')
            
            return redirect(next)

    return render_template('login.html', form=form)

@users_blueprint.route('/register',methods=['GET','POST'])
def register():
    form = RegistrationForm()

    if form.validate_on_submit():
        user = User(email=form.email.data,
                    username=form.username.data,
                    password=form.password.data)

        db.session.add(user)
        db.session.commit()
        flash('Thanks for registration!')
        
        token = generate_confirmation_token(user.email)
        flash(f"token: {token}")
        confirm_url = url_for('users.confirm', token=token, _external=True)
        html = render_template('mail_send.html', confirm_url=confirm_url)
        send_email_confirmation('jpprjn@yahoo.com',html)
        login_user(user)
        #return redirect(url_for('users.login'))
    
        return redirect(url_for('kittens.list'))

    return render_template('register.html',form=form)

@users_blueprint.route('/confirm/<token>')
def confirm(token):
    try:
        email = confirm_token(token)
    except:
        flash('The confirmation link is invalid or has expired')

    user = User.query.filter_by(email=email).first_or_404()
    if user.confirmed:
        flash('Account already confirmed. Please login')
    else:
        user.confirmed = True 
        user.confirmed_on = datetime.datetime.now()
        db.session.add(user)
        db.session.commit()
        flash('You have confirmed your account. Thanks!')

    return redirect(url_for('index'))