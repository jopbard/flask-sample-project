# application/owners/views.py
from flask import Blueprint,render_template,redirect,url_for
from application import db 
from application.models import Owner 
from application.owners.forms import AddForm 

owners_blueprint = Blueprint('owners',__name__,
                                template_folder='templates/owners')

@owners_blueprint.route('/add',methods=['GET','POST'])
def add():

    form = AddForm()
        
    if form.validate_on_submit():
        name = form.name.data 
        kit_id = form.kit_id.data 

        new_owner = Owner(name, kit_id)
        db.session.add(new_owner)
        db.session.commit()    

        return redirect(url_for('kittens.list'))
    
    return render_template('add_owner.html',form=form)