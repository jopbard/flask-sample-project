# forms.py owners 
from flask_wtf import FlaskForm
from wtforms import StringField,IntegerField,SubmitField

class AddForm(FlaskForm):
    name = StringField('Name of Owner:')
    kit_id = IntegerField("Id of Kitten")
    submit = SubmitField("Add Owner")